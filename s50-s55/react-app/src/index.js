import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// Importing react-bootstrap
// npm install react-bootstrap bootstrap (command)
import "bootstrap/dist/css/bootstrap.min.css";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


/*const name = "John Smith";
const element = <h1>Hello, {name}</h1>
// {} -> embedding

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(element);*/