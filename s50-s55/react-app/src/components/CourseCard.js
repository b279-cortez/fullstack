import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

// In React.js we have 3 hooks/states
/*
1. useState
2. useEffect
3. useContext
*/

import { useState } from "react";

export default function CourseCard({courseProp}){

	// checks if props was successfully passed
	console.log(courseProp.name);
	// checks the type of the passed data
	console.log(typeof courseProp);

	// Destructuring the courseProp into their own variables
	const { _id, name, description, price } = courseProp;

	/*// useState
	// Used for storing different states
	// Used to keep track of information to individual components
	// Syntaxt -> const [getter, setter] = useState(initialGetterValue);

	const [count, setCount] = useState(0);
	console.log(useState(0));

	const [seat, setSeat] = useState(30);

	// Function that keeps track of the enrollees for a course
	function enroll(){
		if(seat > 0){
            setCount(count + 1);
            setSeat(seat - 1);
        }else{
            alert("No more seats available!");
        }
        
        console.log("Enrollees: " + count);
        console.log("available Seats: " + seat);
	}*/

	return (
		<Card className="my-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className="btn btn-primary" to={`/courseView/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
	)
}