import { Form, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext"
import { Navigate } from "react-router-dom";


export default function Register(){

	const { user } = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);


	useEffect(() => {
		// Validation to enable register button
		if ((email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password1, password2]);

	function registerUser(e){
		e.preventDefault();

		// clear input fields
		setEmail("");
		setPassword1("");
		setPassword2("");

		alert("Thank you for registering!");
	}

	return (
		(user.token !== null) ?
            <Navigate to="/courses"/>
        :
		<>
			<h1>Registration Page</h1>
			<Form className="my-5" onSubmit={e => registerUser(e)}>

		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control type="email" placeholder="Enter email" onChange={e => setEmail(e.target.value)} value={email} required/>
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" onChange={e => setPassword1(e.target.value)} value={password1} required/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password2">
		        <Form.Label>Confirm Password</Form.Label>
		        <Form.Control type="password" placeholder="Confirm Password" onChange={e => setPassword2(e.target.value)} value={password2} required/>
		      </Form.Group>

		      {/*conditionally render submit button based on isActive state*/}

		      { isActive ?

		      	<Button variant="primary" type="submit" id="submitBtn">
		        	Register
		      	</Button>

		      	:

		      	<Button variant="danger" type="submit" id="submitBtn" disabled>
		        	Register
		      	</Button>

		      }

		      
		    </Form>
	    </>

	)
}