import { Row, Col, Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function ArtworkCard({productProp}){
	
	const { _id, name, description, price, image } = productProp;

	return(

		
		<>
		<Col className="my-5" xs={12} md={6} lg={3}>
			<Card className="artworkCard">
		      <Card.Img variant="top" src="img-fluid product-img-fit w-100 py-3" src={image} />
		      <Card.Body>
		        <Card.Title className="card-title text-center">{name}</Card.Title>
		        <Card.Text className="card-text text-center">php {price}</Card.Text>
		        
		      </Card.Body>
		      <Card.Footer>
		      	<Button  className="catalog-btn" variant="primary" as={Link} to={`/artworkView/${_id}`}>I Want This!</Button>
		      </Card.Footer>
		    </Card>
	    </Col>
		</>
		

	)
}