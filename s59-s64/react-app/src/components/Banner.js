import { Row, Col, Button, Container } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function Banner({bannerProps}){
	
	const { title, content, buttonDestination, buttonLabel } = bannerProps;

	return(

		<>
			<h1 className="banner-title my-3">{title}</h1>
			<p className="banner-subtitle">{content}</p>
			<Button className="banner-button my-3" as={Link} to={buttonDestination}>{buttonLabel}</Button>
		</>

	)
}