import { Nav, Navbar, NavDropdown} from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import { useContext } from "react";
import UserContext from "../UserContext";

export default function AppNavbar(){

  const { user } = useContext(UserContext);

	return (

  	<Navbar variant="dark" expand="lg" className="px-5 py-1 navbar-base" fixed="top">
        <Navbar.Brand className="navbar-brandname" as={Link} to={"/"}>MeArt</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ms-auto nav-link">
                {
                    (user.isAdmin)
                    ?
                    <>
                    <Nav.Link className="px-3" as={NavLink} to="/dashboard" end>Dashboard</Nav.Link>
                    <Nav.Link className="px-3" as={NavLink} to={"/shopArtworks"}>Shop Artworks</Nav.Link>
                    </>
                    :
                    <>
                    <Nav.Link className="px-3" as={NavLink} to={"/"}>Home</Nav.Link>
                    <Nav.Link className="px-3" as={NavLink} to={"/shopArtworks"}>Shop Artworks</Nav.Link>
                    <Nav.Link className="px-3">Contact</Nav.Link>
                    </>
                }

                {
                  (user.token !== null) 
                  ?
                    <>
                    <NavDropdown className="px-3" title={user.email} align="end" id="collasible-nav-dropdown" end>
                    <NavDropdown.Item as={ NavLink } to="/logout" end>Logout</NavDropdown.Item>
                    </NavDropdown>
                    </>
                  :
                    <>
                      <Nav.Link className="px-3" as={NavLink} to={"/register"}>Register</Nav.Link>
                      <Nav.Link className="px-3" as={NavLink} to={"/login"}>Login</Nav.Link>
                    </>
                }
              </Nav>
          </Navbar.Collapse>
      </Navbar>

	)
}