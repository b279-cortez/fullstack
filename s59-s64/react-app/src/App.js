import './App.css';

// components
import AppNavbar from "./components/AppNavbar";

// pages
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Artworks from "./pages/Artworks";
import ArtworkView from "./pages/ArtworkView";
import Dashboard from "./pages/Dashboard";
import CreateNewArtwork from "./pages/CreateNewArtwork";
import EditArtwork from "./pages/EditArtwork";
import Error from "./pages/Error";

// react bootstrap and react router dom
import { Route, Routes } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { Container } from "react-bootstrap";

//
import { useState } from "react";
import { UserProvider } from "./UserContext";


function App() {

    // User state for global scope
    const [user, setUser] = useState({
        id : localStorage.getItem("id"),
        isAdmin : localStorage.getItem("isAdmin"),
        email : localStorage.getItem("email"),
        token : localStorage.getItem("token")
    })

    // For clearing the local storage on logout
    const unsetUser = () => {
        localStorage.clear();
    } 

    return (
        <UserProvider value={{user, setUser, unsetUser}}>
            <Router>
                <AppNavbar/>
                <Routes>
                    <Route path="/" element={<Home/>} />
                    <Route path="/shopArtworks" element={<Artworks/>} />
                    <Route path="/artworkView/:productId" element={<ArtworkView/>} />
                    <Route path="/dashboard" element={<Dashboard/>} />
                    <Route path="/createNewArtwork" element={<CreateNewArtwork/>} />
                    <Route path="/editArtwork/:productId" element={<EditArtwork/>} />
                    <Route path="/register" element={<Register/>} />
                    <Route path="/login" element={<Login/>} />
                    <Route path="/logout" element={<Logout/>} />
                    <Route path="*" element={<Error/>} />
                </Routes>
            </Router>
        </UserProvider>
    )
}

export default App;