import {useState, useContext, useEffect} from "react";
import Swal from "sweetalert2";
import {Container, Form, Button} from "react-bootstrap";
import {useNavigate, Navigate} from "react-router-dom";
import UserContext from "../UserContext";

export default function CreateNewArtwork(){

	const {user} = useContext(UserContext);
	
	const navigate = useNavigate();

	// useState
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stock, setStock] = useState(0);
	const [image, setImage] = useState("");
	const [isButtonActive, setIsButtonActive] = useState(false);

	const addProduct = (event) => {
		event.preventDefault();

		if(user.isAdmin === true){

		fetch(`https://capstone-2-emoc.onrender.com/products/createNewProduct`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				stock: stock,
				image: image
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Artwork successfully added.",
					icon: "success",
					text: `This artwork has been added and is live.`
				})

				navigate("/dashboard");

			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again!"
				})
			}
		})

		setName("");
		setDescription("");
		setPrice(0);
		setStock(0);
		setImage("");

		}else{
			navigate("/");
		}

	}

	useEffect(() => {
		if(name !== "" && description !== "" && price !== "" && image !== ""){
			setIsButtonActive(true);
		}else {
			setIsButtonActive(false);
		}
	}, [name, description, price, image]);

	return (
		(user.isAdmin === true) ?

		<Container className="p-5">
			<h1 className="text-center pb-4 create-artwork-heading">Add New Artwork</h1>
			<Form onSubmit={event => addProduct(event)}>

		      <Form.Group className="mb-3" controlId="name">
		        <Form.Label>Artwork Name</Form.Label>
		        <Form.Control type="text" placeholder="Artwork name" value={name} onChange={event => setName(event.target.value)} />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="description">
		        <Form.Label>Artwork Description</Form.Label>
		        <Form.Control type="text" placeholder="Artwork description" value={description} onChange={event => setDescription(event.target.value)} />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="price">
		        <Form.Label>Price</Form.Label>
		        <Form.Control type="number" placeholder="Artwork price" value={price} onChange={event => setPrice(event.target.value)} />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="stock">
		        <Form.Label>Stock</Form.Label>
		        <Form.Control type="number" placeholder="Artwork stock" value={stock} onChange={event => setStock(event.target.value)} />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="image">
		        <Form.Label>Artwork Image</Form.Label>
		        <Form.Control type="text" placeholder="Artwork image URL" value={image} onChange={event => setImage(event.target.value)} />
		      </Form.Group>

		      {
		      	isButtonActive ?
		      	<Button className="newartwork-btn" variant="primary" type="submit">
			        Create Artwork
			    </Button>
			    :
			     <Button className="newartwork-btn" variant="primary" type="submit" disabled>
			        Create Artwork
			    </Button>
		      }
		     

		    </Form>
		</Container>

		:
		<Navigate to="/"/>
	)
}