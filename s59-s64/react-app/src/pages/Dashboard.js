import UserContext from "../UserContext";
import {useContext, useState, useEffect} from "react";
import {Button, Container, Table} from "react-bootstrap";
import Swal from "sweetalert2";
import Banner from "../components/Banner";
import {Link, Navigate, useNavigate} from "react-router-dom";


export default function Dashboard(){

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	// useState
	const [allProducts, setAllProducts] = useState([]);
	const [productId, setProductId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);
	const [isActive, setIsActive] = useState(false);

	// Get all products
	const getAllProducts = () => {
		fetch(`https://capstone-2-emoc.onrender.com/products/allProducts`, {
			method: "GET",
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(user.isAdmin === true){

			setAllProducts(data.map(product => {
				return (
					<>
						<tr key={product._id}>
							<td  className="table-body">{product._id}</td>
							<td  className="table-body">{product.name}</td>
							<td>{product.description}</td>
							<td  className="table-body">{product.price}</td>
							<td  className="table-body">{product.stocks}</td>
							<td  className="table-body">{product.isActive ? "Active" : "Inactive"}</td>
							<td>
								{
									(product.isActive) ?
										<Button className="table-button" variant="danger" onClick={() => archive(product._id)}>
											Archive
										</Button>
									:
										<>
										<Button className="table-button" variant="success" onClick={()=>unarchive(product._id)}>
											Unarchive
										</Button>

										<Button className="table-button mt-2" as={Link} to={`/editArtwork/${product._id}`}>
											Edit Artwork
										</Button>
										</>
								}
							</td>
						</tr>
					</>
				)
			}))
			}else{
				navigate("/");
			}
		})
	}

	useEffect(() => {
		getAllProducts()
	}, [])

	// Archive product
	const archive = (id) => {
		// console.log(id)
		fetch(`https://capstone-2-emoc.onrender.com/products/${id}/archiveProduct`, {
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				// console.log(data)
				Swal.fire({
					title: "You have successfully archived this product!",
					icon: "success",
					text: "Product archived."
				})

				getAllProducts();

			}else{
				Swal.fire({
					title: "Archive unsuccessful!",
					icon: "error",
					text: "Something went wrong. Please try again."
				})
			}
		})
	}

	// Unarchive product
	const unarchive = (id) => {
		fetch(`https://capstone-2-emoc.onrender.com/products/${id}/unarchiveProduct`, {
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Unarchive successful!",
					icon: "success",
					text: "Product unarchived."
				})

				getAllProducts();

			}else{
				Swal.fire({
					title: "Unarchive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again."
				})
			}
		})
	}

	// Dashboard banner
	const dashboardBanner = {
		title: "Welcome to your Dashboard, Admin!",
		content: "Here is where you can manage your products.",
		buttonDestination: "/createNewArtwork",
		buttonLabel: "Add New Artwork"
	}

	return(
		(user.isAdmin) ?
		<>
		<Container fluid className="dashboard-banner p-5" sm={12}>
			<Banner bannerProps={dashboardBanner}/>
		</Container>

		<Container className="dashboard-table my-5" sm={12}>
			<Table>
				<thead>
					<tr>
						<th className="table-header">ID</th>
						<th className="table-header">Name</th>
						<th className="table-header">Description</th>
						<th className="table-header">Price</th>
						<th className="table-header">Stocks</th>
						<th className="table-header">Status</th>
						<th className="table-header">Actions</th>
					</tr>
				</thead>
				<tbody>
					{allProducts}
				</tbody>
			</Table>
		</Container>
		</>
		:
		<Navigate to="/" />
	)
}