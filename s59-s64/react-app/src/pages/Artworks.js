import ArtworkCard from "../components/ArtworkCard";
import { useState, useEffect } from "react";
import { Row, Container, Col } from "react-bootstrap";



export default function Artworks(){

	const [ActiveArtworks, setActiveArtworks] = useState([]);

	// retrieve the products from db
	useEffect(() => {
		fetch(`https://capstone-2-emoc.onrender.com/products/activeArtworks`)
		.then(res => res.json())
		.then(data => {

			setActiveArtworks(data.map(artwork => {
				return (

					<ArtworkCard key={artwork._id} productProp={artwork} />

				)
			}))

		})
	}, [])

	return (

		<>
		<Container fluid className="artwork-catalog p-5">
			<Row className="py-2">
				<Col className="px-5">
					<p className="sale-text">Special Offer</p>
					<h1 className="sale-title">S A L E</h1>
					<h3 className="sale-subtitle"><strong>Shop Now</strong> and <strong>Save</strong> Up to <strong>50%</strong> for a <strong>Limited Time</strong>!</h3>
				</Col>
			</Row>
		</Container>
		<Container>
			<h1 className="catalog-title pt-5 text-center">Artwork Products</h1>
			<Row>
				{ActiveArtworks}
			</Row>
		</Container>
		</>

	)
}