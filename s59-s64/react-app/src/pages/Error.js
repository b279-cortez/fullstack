import Banner from "../components/Banner";
import {Container} from "react-bootstrap";

export default function Error(){

	const data = {
		title: "404 - NOT FOUND",
		content: "The page you are looking for cannot be found.",
		buttonDestination: "/",
		buttonLabel: "Home"
	}

	return (
		<Container fluid className="p-5 error-banner text-center">
			<Banner bannerProps={data} />
		</Container>
	)

}