import {useState, useEffect, useContext} from "react";
import Swal from "sweetalert2";
import {Container, Form, Button} from "react-bootstrap";
import {useParams} from "react-router-dom";
import UserContext from "../UserContext";
import {Navigate, useNavigate} from "react-router-dom";


export default function EditArtwork(){

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();

	// useState
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stock, setStock] = useState(0);
	const [image, setImage] = useState("");

	useEffect(() => {

		console.log(productId)

		if(user.isAdmin === true){

		fetch(`https://capstone-2-emoc.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setImage(data.image);

			console.log(data);
		})
		}else{
			navigate("/");
		}
	}, [productId])

	const editProduct = (event) => {
		event.preventDefault();

		if(user.isAdmin === true){

		fetch(`https://capstone-2-emoc.onrender.com/products/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				stock: stock,
				image: image
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data){
				Swal.fire({
					title: "Product edit successful",
					icon: "success",
					text: `This product was edited successfully.`
				})

				navigate("/dashboard");

			}else{
				Swal.fire({
					title: "Product edit unsuccessful",
					icon: "error",
					text: "Please try again later"
				})
			}
		})

		setName("");
		setDescription("");
		setPrice(0);
		setStock(0);
		setImage("");

		}else{
			navigate("/");
		}
	}

	return (
		(user.isAdmin === true) ?

		<Container className="p-5">
			<h1 className="text-center pb-4 create-artwork-heading">Edit Artwork</h1>
			<Form onSubmit={event => editProduct(event)}>

		      <Form.Group className="mb-3" controlId="name">
		        <Form.Label>Artwork Name</Form.Label>
		        <Form.Control type="text" placeholder="Artwork name" value={name} onChange={event => setName(event.target.value)} />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="description">
		        <Form.Label>Artwork Description</Form.Label>
		        <Form.Control type="text" placeholder="Artwork description" value={description} onChange={event => setDescription(event.target.value)} />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="price">
		        <Form.Label>Price</Form.Label>
		        <Form.Control type="number" placeholder="Artwork price" value={price} onChange={event => setPrice(event.target.value)} />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="stock">
		        <Form.Label>Stock</Form.Label>
		        <Form.Control type="number" placeholder="Artwork stock" value={stock} onChange={event => setStock(event.target.value)} />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="image">
		        <Form.Label>Artwork Image</Form.Label>
		        <Form.Control type="text" placeholder="Artwork image URL" value={image} onChange={event => setImage(event.target.value)} />
		      </Form.Group>

		      <Button className="newartwork-btn" variant="primary" type="submit">
		        Edit Artwork
		      </Button>

		    </Form>
		</Container>

		:
		<Navigate to="/"/>
	)
}