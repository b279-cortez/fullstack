import Banner from "../components/Banner"
import {Container, Row, Col, Card, Button} from "react-bootstrap";
import {Link} from "react-router-dom";


export default function Home(){

	const bannerContent = {
		title: "See the World Through my Artworks",
		content: "Explore the world's wonders and emotions through my art.",
		buttonDestination: "/shopArtworks",
		buttonLabel: "Shop Now~"
	}

	return (

		<>
		{/*landing Page*/}
		<Container fluid className="p-2 p-md-4 p-lg-5 landing-page">
			<Row className="p-2 p-md-4 p-lg-5">
				<Col className="p-2 p-md-4 p-lg-5">
					<Banner bannerProps={bannerContent}/>
				</Col>
			</Row>
		</Container>

		{/*Hot Deals*/}
		<Container>
			<h2 className="hotdeals-title mt-5 py-4 text-center">ONGOING HOT DEALS!</h2>
			<Row>
				{/*Hot deal #1*/}
				<Col className="p-4" sm={12} lg={6}>
					<Card className="hotdeals">
			            <Card.Img variant="top" src="https://images.unsplash.com/photo-1588534724418-73b75d412ce5?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" />
			            <Card.Body>
			              <Card.Title className="pb-2">Limited Edition Print Bundle</Card.Title>
			              <Card.Subtitle>Description:</Card.Subtitle>
			              <Card.Text>
			                Get a limited edition bundle of three high-quality art prints.
			              </Card.Text>

			              <Card.Subtitle>Offer:</Card.Subtitle>
			              <Card.Text>
			                Buy two prints, get one free.
			              </Card.Text>

			              <Card.Subtitle>Features:</Card.Subtitle>
			              <Card.Text>
			                <ul>
			                	<li>Limited edition prints of popular artworks.</li>
			                	<li>Different sizes and styles available.</li>
			                	<li>Free shipping worldwide.</li>
			                	<li>Each print comes with a certificate of authenticity.</li>
			                </ul>
			              </Card.Text>
			            </Card.Body>
			        </Card>
				</Col>

				{/*Hot deal #2*/}
				<Col className="p-4" sm={12} lg={6}>
					<Card className="hotdeals">
			            <Card.Img variant="top" src="https://images.unsplash.com/photo-1465161191540-aac346fcbaff?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" />
			            <Card.Body>
				            <Card.Title className="pb-2">Seasonal Sale</Card.Title>
				              <Card.Subtitle>Description:</Card.Subtitle>
				              <Card.Text>
				                Enjoy discounted prices on selected artworks for a limited time.
				              </Card.Text>

				              <Card.Subtitle>Offer:</Card.Subtitle>
				              <Card.Text>
				                Up to 40% off on a wide range of artworks.
				              </Card.Text>

				              <Card.Subtitle>Features:</Card.Subtitle>
				              <Card.Text>
				                <ul>
				                	<li>Extensive collection of diverse art styles and genres.</li>
				                	<li>Discounted prices on selected artworks for a limited period.</li>
				                	<li>Variety of mediums available, including paintings, photography, and sculptures.</li>
				                	<li>Opportunity to discover new artists and add unique pieces to your collection.</li>
				                </ul>
				              </Card.Text>
			            </Card.Body>
			        </Card>
				</Col>
			</Row>
			
		</Container>

		{/*Shop now button below hotdeals*/}

		<Container className="hotdeals-button-container pt-3 pb-5">
			<Button className="hotdeals-button" as={Link} to={`/shopArtworks`}>
				Shop Now!
			</Button>
		</Container>

		</>

	)
}