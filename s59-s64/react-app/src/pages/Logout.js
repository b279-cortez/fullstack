import { useContext, useEffect } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";


export default function Logout(){
	
	const { unsetUser, setUser } = useContext(UserContext);

	// to clear the local storage
	unsetUser();

	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null,
			email: null,
			token: null
		})
	})

	return (

		<Navigate to="/login" />

	)
}