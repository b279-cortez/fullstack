import { Form, Button, Container, Row } from "react-bootstrap";
import UserContext from "../UserContext";
import { useContext, useState, useEffect } from "react";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";


export default function Login(){

	const {user, setUser} = useContext(UserContext);

	// useState
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isButtonActive, setIsButtonActive] = useState(false);

	// Validation to enable submit button when all fields are populated with both passwords match
	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsButtonActive(true);
		}else {
			setIsButtonActive(false);
		}
	}, [email, password])

	function loginUser(event) {

		// prevent page redirection upon form submission
		event.preventDefault();

		// login user
		fetch(`https://capstone-2-emoc.onrender.com/users/login`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data.access !== undefined){
				localStorage.setItem("token", data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login successful!",
					icon: "success",
					text: "It's great to have you back!"
				})
			}else {
				Swal.fire({
					title: "Oops, please try again.",
					icon: "error",
					text: "Make sure your email and password are correct."
				})
			}
		})

		// Clear input fields after logging in
		setEmail("");
		setPassword("")
	}


	// Retrieve user details using user token
	const retrieveUserDetails = (token) => {
		fetch(`https://capstone-2-emoc.onrender.com/users/userDetails`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			localStorage.setItem("id", data._id);
			localStorage.setItem("isAdmin", data.isAdmin);
			localStorage.setItem("email", data.email);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin,
				email: data.email
			})
		})
	}

	return(
		(user.token !== null) ?
			<Navigate to="/" />
		:
		<>
		<Container className="p-sm-5 p-lg-5 mx-lg-5">
			<Row className="p-sm-5 p-lg-5 mx-lg-5">
				<h1 className="text-center pb-5 login-title">Login</h1>
				<Form onSubmit={event => loginUser(event)}>

			      <Form.Group controlId="userEmail">
			        <Form.Label className="pb-0 mb-0">Email</Form.Label>
			        <Form.Control type="email" placeholder="Your email address" value={email} onChange={event => setEmail(event.target.value)} required />
			      </Form.Group>

			      <Form.Group controlId="password" className="mt-2">
			        <Form.Label className="pb-0 mb-0">Password</Form.Label>
			        <Form.Control type="password" placeholder="Your password" value={password} onChange={event => setPassword(event.target.value)} required />
			      </Form.Group>

			      {
			      	isButtonActive ?
			      		<Button variant="primary" type="submit" id="submitBtn" className="mt-4">
			        		Login
			      		</Button>
			      	:
			      		<Button variant="secondary" type="submit" id="submitBtn" disabled className="mt-4">
					    	Login
					    </Button>
			      }
				     
				</Form>
			</Row>
		</Container>
		</>
	)
}