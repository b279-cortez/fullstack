import UserContext from "../UserContext";
import {useContext, useState, useEffect} from "react";
import {Button, Row, Col, Container, Form} from "react-bootstrap";
import {useParams, useNavigate, Navigate, Link, useHistory} from "react-router-dom";
import Swal from "sweetalert2";

export default function ArtworkView(){

	// contains the user details from the local storage.
	const {user} = useContext(UserContext);

	// allows to retrieve the productId passed via URL
	const {productId} = useParams();

	// where to navigate after successfully buying product/artwork
	const navigate = useNavigate();

	// useState
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [quantity, setQuantity] = useState(1);
	const [image, setImage] = useState("");

	// fetch the data of certain product
	useEffect(() => {

		console.log(productId)

		fetch(`https://capstone-2-emoc.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setImage(data.image);

			console.log(data);
		})
	}, [productId])


	// function to create an order
	const buy = (productId) => {
		if(localStorage.getItem("isAdmin") === "true"){
			Swal.fire({
				title: "Admin Access Denied",
				icon: 'error',
				text: "Admins cannot perform this action."
			});

			navigate("/");

			console.log(localStorage.getItem("isAdmin"));
		}else{
			fetch(`https://capstone-2-emoc.onrender.com/users/createOrder`, {
				method: "POST",
				headers: {
					"Content-type" : "application/json",
					Authorization : `Bearer ${localStorage.getItem("token")}`
				},
				body: JSON.stringify({
					productId: productId,
					quantity: quantity
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);


				if(data === true){
					Swal.fire({
						title: "Order placed successfully!",
						icon: 'success',
						text: "Thank you for your order."
					});

				navigate("/");

				}else{
					Swal.fire({
						title: "Something went wrong!",
						icon: 'error',
						text: "Please try again!"
					});
				}
			})
		}
	}


	return (

		<Container className="my-5 artwork-view">
			<Row>
				<Col sm={12} md={6} className="p-5">
					<img className="img-fluid artwork-view-img my-4" src={image} />
				</Col>

				<Col sm={12} md={6} className="p-5 artwork-view-details">
					<h2 className="mb-5">{name}</h2>
					<h5>Description:</h5>
					<p>{description}</p>
					<h5>Price:</h5>
					<p>Php {price}</p>
					<h5>How many of this product do you want?</h5>
					<Form.Control type="number" placeholder="Quantity" value={quantity} onChange={event => setQuantity(event.target.value)} />
					{
				
						(user.id !== null)
						?
							<Button id="checkOutBtn" className='w-50 mt-4 shadow artwork-view-button' onClick={() => buy(productId)}>CHECK OUT</Button>
						:
							<Button className='w-50 mt-4 shadow artwork-view-button' as={Link} to={`/login`}>LOGIN TO BUY</Button>
					}
				</Col>
			</Row>
		</Container>
	)
}